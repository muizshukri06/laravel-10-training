<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::all();

        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name'          => 'required',
            'description'   => 'required',
            'price'         => 'required',
            'stock'         => 'required',
        ],[
            'name.required'          => 'Name is required',
            'description.required'   => 'Description is required',
            'price.required'         => 'Price is required',
            'stock.required'         => 'Stock is required',
        ]);
        
        try {
            $product = Product::create($validate);
            
            return redirect(route('products'))->withSuccess('Product has been saved!');
        } catch (\Throwable $th) {
            return back()->withError('Something when wrong!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::find($id);

        return view('products.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::find($id);

        return view('products.edit')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validate = $request->validate([
            'name'          => 'required',
            'description'   => 'required',
            'price'         => 'required',
            'stock'         => 'required',
        ],[
            'name.required'          => 'Name is required',
            'description.required'   => 'Description is required',
            'price.required'         => 'Price is required',
            'stock.required'         => 'Stock is required',
        ]);
        
        try {
            Product::find($id)->update($validate);

            return redirect(route('products'))->withSuccess('Product has been updated!');
        } catch (\Throwable $th) {
            return back()->withError('Something when wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            Product::find($id)->delete();
            return redirect(route('products'))->withSuccess('Product has been deleted!');
        } catch (\Throwable $th) {
            return back()->withError('Something when wrong!');
        }
    }
}
