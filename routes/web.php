<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard');
// });

// Route::get('/',[App\Http\Controllers\DashboardController::class,'index']);
Route::get('/',[DashboardController::class,'index'])->name('dashboard');
Route::get('/home',[DashboardController::class,'index'])->name('dashboard');

// Route::get('/products', function () {
//     return "<p>THIS IS PRODUCTS PAGE</p>";
// });

Route::get('/products',[ProductController::class,'index'])->name('products');
Route::get('/products/create',[ProductController::class,'create'])->name('products.create');
Route::post('/products/store',[ProductController::class,'store'])->name('products.store');
Route::get('/products/show/{id}',[ProductController::class,'show'])->name('products.show');
Route::get('/products/edit/{id}',[ProductController::class,'edit'])->name('products.edit');
Route::put('/products/update/{id}',[ProductController::class,'update'])->name('products.update');
Route::delete('/products/delete/{id}',[ProductController::class,'destroy'])->name('products.destroy');

// Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
