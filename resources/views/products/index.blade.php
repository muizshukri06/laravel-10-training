@extends('layouts.app')

@section('header')
    <b>Product List</b>
@endsection

@section('content')
    <div class="d-grid gap-2 d-md-flex justify-content-md-end pb-2">
        <a class="btn btn-success btn-sm" href="{{ url(route('products.create')) }}">
        <i class="bi bi-plus-circle-fill"></i> Add product
        </a>
    </div>
    <table class="table table-striped table-dark">
        <thead>
            <tr>
            <th scope="col">#</th>
            <!-- <th scope="col">ID</th> -->
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Price</th>
            <th scope="col">Stock</th>
            <th scope="col" class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody class="table-light">
            @php
                $index = 1
            @endphp
            @forelse ($products as $product)
            <tr id="product-{{ $product->id }}">
                <th scope="row">{{ $index++ }}</th>
                <!-- <td>{{ $product->id }}</td> -->
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->stock }}</td>
                <td>
                    <div class="d-flex justify-content-around">
                        <a class="btn" href="{{ url(route('products.show', $product->id)) }}">
                            <i class="bi bi-file-earmark-fill text-primary"></i>
                        </a>
                        <a class="btn" href="{{ url(route('products.edit', $product->id)) }}">
                            <i class="bi bi-pencil-fill text-warning"></i>
                        </a>
                        <form onsubmit="return confirm('Are you sure you want to delete {{ $product->name }}?');" action="{{ url(route('products.destroy', $product->id)) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-link">
                                <i class="bi bi-trash-fill text-danger"></i>
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr>
                <th scope="row" colspan="6" class="text-center">wow so empty...</th>
            </tr>
            @endforelse
    </table>
@endsection