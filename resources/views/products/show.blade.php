@extends('layouts.app')

@section('header')
    <a style="text-decoration:none;color:black;" href="{{ url(route('products')) }}">
        <i class="bi bi-arrow-left"></i>
    </a>
    <b>{{ $product->name }}</b>
@endsection

@section('content')
    <div class="container">
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" value="{{ $product->name }}" readonly>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea type="text" class="form-control" readonly>{{ $product->description }}</textarea>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <div class="input-group mb-3">
                <span class="input-group-text">RM</span>
                <input type="text" class="form-control" value="{{ $product->price }}" readonly>
            </div>
        </div>
        <div class="mb-3">
            <label for="stock" class="form-label">Stock</label>
            <input type="text" class="form-control" value="{{ $product->stock }}" readonly>
        </div>
    </div>
@endsection